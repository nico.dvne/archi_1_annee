#include "C:\Users\nicod\OneDrive\Bureau\Cours\Semestre_2\Architecture_Programmation\TP\TP_ALARME\main.h"

#define c1h  output_high(pin_d0)
#define c1l  output_low(pin_d0)
#define c2h  output_high(pin_d1)
#define c2l  output_low(pin_d1)
#define c3h  output_high(pin_d2)
#define c3l  output_low(pin_d2)

#define l1  input(pin_d3)
#define l2  input(pin_d4)
#define l3  input(pin_d5)
#define l4  input(pin_d6)

//Capteurs instantaneees
#define capteur1 input(pin_b1)
#define capteur2 input(pin_b2)
#define capteur3 input(pin_b3)
#define capteur4 input(pin_b4)

//Capteurs retardes
#define capteur5 input(pin_b5)
#define capteur6 input(pin_b6)

//Allumage Extinction Buzzer
#define buzzer_ON output_high(pin_a0)
#define buzzer_OFF output_low(pin_a0)

//Allumage Extinction Sirene Exterieur
#define sirene_ON output_high(pin_a1)
#define sirene_OFF output_low(pin_a1)

//Alarme en marche ou non
#define alarme_ARMEE output_high(pin_c0)
#define alarme_DESARMEE output_low(pin_c0)

//Boucle
#define boucle1_ON output_high(pin_c1)//s'allume si le capteur1 fonctionne ou a fonctionner. Se desactive quand le code de desarmation est saisie
#define boucle1_OFF output_low(pin_c1)

#define boucle2_ON output_high(pin_c2)//s'allume si le capteur2 fonctionne ou a fonctionner. Se desactive quand le code de desarmation est saisie
#define boucle2_OFF output_low(pin_c2)

#define boucle3_ON output_high(pin_c5)//s'allume si le capteur3 fonctionne ou a fonctionner. Se desactive quand le code de desarmation est saisie
#define boucle3_OFF output_low(pin_c5)

#define boucle4_ON output_high(pin_c4)//s'allume si le capteur4 fonctionne ou a fonctionner. Se desactive quand le code de desarmation est saisie
#define boucle4_OFF output_low(pin_c4)

#define boucle5_ON output_high(pin_e0)//s'allume si le capteur5 fonctionne ou a fonctionner. Se desactive quand le code de desarmation est saisie
#define boucle5_OFF output_low(pin_e0)

#define boucle6_ON output_high(pin_e1)//s'allume si le capteur6 fonctionne ou a fonctionner. Se desactive quand le code de desarmation est saisie
#define boucle6_OFF output_low(pin_e1)


int intrusion1=0,intrusion2=0,intrusion3=0,intrusion4=0,intrusion5=0,intrusion6=0;

int16 code_saisi, code_armement=33,code_desarmement=1664;
int16 retard_sortie=5,retard_entree=6;
int16 sec;
int16 t1,t1b;
int16 t2,t2b;
int16 t3,t3b;
int16 n;
int1 crb,alarme_active;
int1 crb2,crb3;
int sirene=0;
int activation=0;
int1 retardement=0;
int16 delay_max=10;
int1 dejasonne=0;

#int_TIMER1
void  TIMER1_isr(void) 
{ int8 dix;
  set_timer1(3036); //on passe ici tous les dixiemes de seconde
  dix++;
  if(dix==10) {
   
      // on passe ici toutes les secondes
      sec++;
      dix=0;
      t1--; 
      if(retardement==1 && t2>0)
        { t2--;
         printf("t2 t2\n\r");
          }
      if(sirene==1 && t3>0)
         t3--;
      
             }

}

#int_EXT
void  EXT_isr(void) 
{ //lecture colonne1
    c1h;c2l;c3l;
    if (l1){
        printf("1");
        n=10*n+1;
    }
    if (l2){
        printf("4");
        n=10*n+4;
       
        
    }
    if (l3){
        printf("7");
        n=10*n+7;
    }
    if (l4){
        printf("*");
         
    }
    //lecture colonne2
    c1l;c2h;c3l;
    if (l1){
        printf("2");
        n=10*n+2;
    }
    if (l2){
        printf("5");
        n=10*n+5;
    }
    if (l3){
        printf("8");
        n=10*n+8;
    }
    if (l4){
        printf("0");
        n=10*n; 
    }
    
     //lecture colonne3
    c1l;c2l;c3h;
    if (l1){
        printf("3");
        n=10*n+3;
    }
    if (l2){
        printf("6");
        n=10*n+6;
    }
    if (l3){
        printf("9");
        n=10*n+9;
    }
    if (l4){
        code_saisi=n;n=0;
        printf("\n\r");
        //printf("code saisi:%lu\n\r",code_saisi);
        if (code_saisi==code_armement) {
               printf("alarme armee dans %lu secondes \n\r",retard_sortie);
               t1=retard_sortie;t1b=t1;
               t2=retard_entree; t2b=t2;
               t3=delay_max;t3b=t2;
               boucle1_OFF;boucle2_OFF;boucle3_OFF;boucle4_OFF;boucle5_OFF;boucle6_OFF;
               
               
               crb=1;
               crb2=1;
               crb3=1;
               
             
        } else if (code_saisi==code_desarmement) {
                    printf("alarme desarmee\n\r");
                    sirene=0;
                    alarme_DESARMEE;
                    sirene_off;
                    buzzer_off;
               }else {
                    printf("Erreur code\n\r"); 
        
               }
        
    }
    c1h;c2h;c3h;
}



void main()
{

   setup_adc_ports(NO_ANALOGS);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);
   setup_timer_2(T2_DISABLED,0,1);
   enable_interrupts(INT_TIMER1);
   enable_interrupts(INT_EXT);
   enable_interrupts(GLOBAL);
//Setup_Oscillator parameter not selected from Intr Oscillator Config tab

   // TODO: USER CODE!!
   c1h;c2h;c3h;
   
   while(true) { 
                  if (crb && t1>0 && t1b!=t1) {
                      buzzer_on;
                      t1b=t1;
                      putc(12);
                      printf("alarme armee dans %lu secondes \n\r",t1);
                      }  
                  if (crb && t1<=0) {
                     putc(12);
                     printf("Alarme Armee\n\r");
                      buzzer_off;
                      crb=0;
                      alarme_active=1;
                      alarme_ARMEE;
                      sirene=0;
                  }                      
                  if(alarme_active==1)
                  {
                     if(capteur1==0 )
                     {
                           activation=1;
                           boucle1_ON;
                           if(sirene!=1)sirene=1;
                           if(intrusion1==0)
                              {
                                 printf("Intrusion zone 1 \n\r");
                                 intrusion1=1;
                              }
                     }                       
                     if(capteur2==0)
                     {
                        activation=1;
                        boucle2_ON;
                        if(sirene!=1)sirene=1;
                        if(intrusion2==0)
                              {
                                 printf("Intrusion zone 2 \n\r");
                                 intrusion2=1;
                              }
                        //if(dejasonne==1){sirene=1;dejasonne=0;}
                     }
                     if(capteur3==0)
                     {
                           activation=1;
                           boucle3_ON;
                           if(sirene!=1)sirene=1;
                           if(intrusion3==0)
                              {
                                 printf("Intrusion zone 3 \n\r");
                                 intrusion3=1;
                              }
                     }
                     if(capteur4==0)
                     {
                        activation=1;
                        boucle4_ON;
                        if(sirene!=1)sirene=1;
                        if(intrusion4==0)
                              {
                                 printf("Intrusion zone 4 \n\r");
                                 intrusion4=1;
                              }
                        
                     }
                     if(capteur5==0)
                     {
                        if(retardement!=1){retardement=1;}
                       
                         if (retardement && crb2 && t2>0 && t2b!=t2) { //  PB AVEC LE T2B ET LE FAIT QUE CA SE DECREMENTE PAS
                            buzzer_on;
                              t2b=t2;  
                              putc(12);
                           printf("sonnerie dans %lu secondes \n\r",t2);
                                                       }  
                             if(t2==0){
                              buzzer_off;
                               if(sirene!=1)sirene=1;
                              boucle5_ON;
                              retardement=0;
                              
                              if(intrusion5==0)
                              {
                                 printf("Intrusion zone 5 \n\r");
                                 intrusion5=1;
                              }
                           }                                    
                        
                     }
                     if(capteur6==0)//penser a ajouter les delais
                     {
                        
                        if(retardement!=1){retardement=1;}
                       
                         if (retardement && crb2 && t2>0 && t2b!=t2) { //  PB AVEC LE T2B ET LE FAIT QUE CA SE DECREMENTE PAS
                            buzzer_on;
                              t2b=t2;  
                              putc(12);
                           printf("sonnerie dans %lu secondes \n\r",t2);
                                                       }  
                             if(t2==0){
                              
                              buzzer_off;
                            
                               if(sirene!=1)sirene=1;
                              boucle6_ON;
                              retardement=0;
                              
                              if(intrusion6==0)
                              {
                                 printf("Intrusion zone 6 \n\r");
                                 intrusion6=1;
                              }
                           }                 
                        
                     }
                   
                  
                  if(sirene==1)
                  {
                     if (crb3 && t3>=0 && t3b!=t3)
                     {
                        putc(12);
                        t3b=t3;
                        printf("Encore %lu secondes de sonnerie\n\r",t3);
                        
                     }
                     
                     if(t3>0)sirene_on;
                     if(t3==0){sirene=0;dejasonne=1;}
                     
                     
                  }
                  else
                  {
                     sirene_off;
                  }
                  }
                                         
                     
}
}                     



